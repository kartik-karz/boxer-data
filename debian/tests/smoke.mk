# TODO: handle arch-specific nodes lxp5 gateway
# TODO: handle suite stretch
NODES = $(filter-out %/lxp5.yml %/gateway.yml,\
 $(wildcard /usr/share/boxer/buster/nodes/*.yml))

all: $(NODES)

$(NODES):
	$(eval suite = /$(subst $() ,/,$(wordlist 1,4,$(subst /,$() ,$@))))
	$(eval node = $(basename $(notdir $@)))
	boxer compose --skel debian/tests/skel --datadir "$(suite)" "$(node)"
	bash ./script.sh
	rm preseed.cfg script.sh
